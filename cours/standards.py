CARDS = [(color, number) for color in ["blue", "red", "green", "yellow"] for number in range(10)]


def display_collection(collection: list):
    for index, element in enumerate(collection):
        print(f"Index {index} ; Element {element}")


def reverse_list_standard(collection: list):
    return reversed(collection)


def reserve_list_for(collection: list):
    new = []
    for i in collection:
        new.insert(0, i)
    return new


def reserve_list_index(collection: list):
    return [collection[-x - 1] for x in range(len(collection))]


def reserve_list_index_dev(collection: list):
    new = []  # on créé la nouvelle liste qu'on va renvoyer à l'utilisateur
    for i in range(len(collection)):  # i va prendre la valeur de l'index de chaque l'élément dans la liste
        new.append(collection[-i - 1])  # on ajoute la valeur situé à l'opposé de l'index récupéré et on lui soustrait 1
    desc = """
    pour une meilleure explication, il va falloir faire un exemple
    en python, imagions que nous avons une liste que l'on va appeler 'collection'
    collection[0], va retourner le premier élément, collection[1], le second, etc...
    à l'inverse, collection[-1], permet d'obtenir le dernier élément, collection[-2], l'avant dernier élément, etc...
    ainsi donc, si on veut inverser une valeur, prendre l'opposé de l'index ne suffit pas
    collection[2] retourne le troisième élément de la liste, là où collection[-2] retourne le second avant dernier élément
    de la liste
    il faut donc ensuite soustraire un pour avoir le bon opposé
    collection[2] retourne le troisième élément de la list, là où collection[-2-1] donc collection[-3] retourne le troisème
    avant dernier élément, nous permettant donc ainsi d'inverser la liste
    """
    return new  # on retourne donc la liste complètement inversée


def recursive_reverse_list(collection: list):
    if len(collection) < 2:  # si la liste contient moins que deux éléments, alors il n'y à absolument rien à inverser
        return collection  # on renvoie donc la liste telle qu'elle a été donnée, exemple : [3] inversé est toujours [3]
    elif len(collection) == 2:  # si la liste contient seulement deux éléments, alors on les inverse
        collection[0], collection[1] = collection[1], collection[0]  # exemple : [3, 2] inversé sera donc [2, 3]
        return collection  # on renvoie la liste inversée
    else:  # si la liste n'est ni inférieure à deux, ni égale à deux (donc supérieur à 2), elle s'exécute
        first = collection.pop(0)  # on retire le premier élément de la liste (pop retire un élément de la liste)
        # et permet également de le récupérer au même moment, dans notre cas pour récupérer le premier élément on
        # utilise donc l'index zéro
        recursive_reverse_list(collection)  # une fois notre premier élément retiré, la liste amputée de son premier
        # élément peut donc repasser le test → cela sera mieux expliqué dans l'exemple
        collection.append(first)  # une fois la liste inversée, on rajoute le premier élément précéda ment retiré
        return collection  # on renvoie la correction


"""
La fonction ci dessus adopte une nouvelle thématique assez importante dans l'utilisation des fonctions en programmation,
j'ai nommé la récursivité. Le principe récursif dans l'idée est assez simple. Une fonction récursive est une fonction
qui peut se rappeler d'elle même.

Dans la fonction ci dessus, la fonction va se rappeler d'elle même à partir du moment où le nombre de valeurs contenues
dans la liste est supérieur à 2.

Exemple :

Je vais utiliser les accolades {} de façon à ce qu'il soit plus compréhensible de connaître le status récursif actuel,
soit basiquement le fait que la fonction se rappelle d'elle même.

On donne une liste collection_0 telle quelle : ["bonjour", "bonsoir", "au revoir", "ainsi donc", "me voici"]

{
    collection_0 possède 5 éléments, ce qui est plus grand que deux.
    Ainsi donc, collection_1 est créé, pour lequel sa première valeur first_0 de collection_0 est amputée.
    first_0 contient donc la valeur "bonjour".
    On se retrouve donc avec collection_1 telle que : ["bonsoir", "au revoir", "ainsi donc", "me voici"]
    
    {
        collection_1 possède 4 éléments, ce qui est plus grand que deux.
        Ainsi donc, collection_2 est créé, pour lequel sa première valeur first_1 de collection_1 est amputée.
        first_1 contient donc la valeur "bonsoir".
        On se retrouve donc avec collection_2 telle que ["au revoir", "ainsi donc", "me voici"]
        
        {
            collection_2 possède 3 éléments, ce qui est plus grand que deux.
            Ainsi donc, collection_3 est créé, pour lequel sa première valeur first_2 de collection_2 est amputée.
            first_2 contient donc la valeur "au revoir".
            On se retrouve donc avec collection_3 telle que ["ainsi donc", "me voici"]
            
            {
                collection_3 possède 2 éléments, ce qui est égal à deux.
                Ainsi donc, son premier élément et son dernier élément sont inversés.
                collection_3 passe de ["ainsi donc", "me voici"] à ["me voici", "ainsi donc"]
            }
            
            On ajoute ensuite first_2 qui contient donc "au revoir" à la fin de collection_3.
            On obtient donc ["me voici", "ainsi donc", "au revoir"].
        }
        
        On ajoute ensuite first_1 qui contient donc "bonsoir" à la fin de collection_2.
        On obtient donc ["me voici", "ainsi donc", "au revoir", "bonsoir"].
    }
    
    On ajoute ensuite first_0 qui contient donc "bonjour" à la fin de collection_1.
    On obtient donc ["me voici", "ainsi donc", "au revoir", "bonsoir", "bonjour"].
}

On peut vérifier, la liste ["me voici", "ainsi donc", "au revoir", "bonsoir", "bonjour"] est belle et bien la version
inversée de la liste ["bonjour", "bonsoir", "au revoir", "ainsi donc", "me voici"].
"""


if __name__ == '__main__':
    reserve_list_index_dev(["bonjour", "bonsoir", "au revoir", "ainsi donc", "me voici"])
